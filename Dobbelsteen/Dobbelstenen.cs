﻿namespace Dobbelsteen
{
    internal class Dobbelstenen
    {
        //Attributen
        private int _aantalZijden;
        private int _waarde;
        private Random _willGetal;

        //Constructor
        public Dobbelstenen(int aantalZijden, Random r)
        {
            Aantalzijden = aantalZijden;
            WillGetal = r;
        }
        public Dobbelstenen(Random r)
        {
            WillGetal = r;
            Aantalzijden = 6;
        }
        public Dobbelstenen()
        {

            Aantalzijden = 6;
            WillGetal = new Random();
        }


        //Properties
        public int Aantalzijden
        {
            get { return _aantalZijden; }
            set { _aantalZijden = value; }
        }
        public int Waarde
        {
            get { return _waarde; }
            set { _waarde = value; }
        }
        public Random WillGetal
        {
            get { return _willGetal; }
            set { _willGetal = value; }
        }
        //Methode
        public void Roll()
        {
            Waarde = WillGetal.Next(1, Aantalzijden + 1);
        }

    }
}
