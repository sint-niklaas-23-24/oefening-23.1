﻿using System.Windows;

namespace Dobbelsteen
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }
        Random random = new Random();
        private void btnGooien_Click(object sender, RoutedEventArgs e)
        {
            Dobbelstenen dblBlauw = new Dobbelstenen();
            Dobbelstenen dblRood = new Dobbelstenen(4, random);
            dblRood.Roll();
            dblBlauw.Roll();
            lblRol1.Content = dblRood.Waarde;
            lblRol2.Content = dblBlauw.Waarde;
        }
    }
}